# ansible-ci

## Build

This image is build using Kaniko, only for amd64 arch.

Packed with required alpine packages

| Package name   | Version   |
| -------------- | --------- |
| ansible        | 7.5.0-r0  |
| git            | 2.40.1-r0 |
| py3-pip        | 23.1.2-r0 |
| openssh-client | 9.3_p2-r0 |
| sshpass        | 1.10-r0   |

Python packages

| Package name | Version |
| ------------ | ------- |
| kubernetes   | 28.1.0  |
