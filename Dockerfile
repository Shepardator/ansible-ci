FROM alpine:3.18
RUN apk add ansible=7.5.0-r0 git=2.40.1-r0 py3-pip=23.1.2-r0 openssh-client=9.3_p2-r0 sshpass=1.10-r0 --update --no-cache
RUN pip3 install kubernetes==28.1.0
RUN ln -sf python3 /usr/bin/python